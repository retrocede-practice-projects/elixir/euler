defmodule Euler.One do
  @moduledoc """
  If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

  Find the sum of all the multiples of 3 or 5 below 1000.
  """

  @doc """
  Finds sum of multiples of 3 or 5 up to (but not including) the limit arg.

  ## Examples
      iex> Euler.One.solve(10)
      23
  """
  @spec solve(integer()) :: integer()
  def solve(limit) do
    solve(Enum.to_list(1..limit-1), 0)
  end

  defp solve([head|tail], acc) when rem(head, 3) == 0 or rem(head, 5) == 0 do
    solve(tail, head + acc)
  end

  defp solve([_|tail], acc) do
    solve(tail, acc)
  end

  defp solve([], acc) do
    acc
  end
end
