defmodule EulerTest.One do
  use ExUnit.Case
  doctest Euler.One

  test "30" do
    assert Euler.One.solve(30) == 195
  end

  test "50" do
    assert Euler.One.solve(50) == 543
  end

  test "100" do
    assert Euler.One.solve(100) == 2318
  end

  test "1000" do
    assert Euler.One.solve(1000) == 233168
  end
end
